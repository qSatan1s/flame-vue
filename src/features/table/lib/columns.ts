

export const miniColumns = [
  {
    title: 'name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'height',
    dataIndex: 'height',
    key: 'height',
  },
  {
    title: 'mass',
    dataIndex: 'mass',
    key: 'mass',
  },
  {
    title: 'hair_color',
    dataIndex: 'hair_color',
    key: 'hair_color',
  },
  { title: 'Action',
    key: 'action',
    width: 250,
  },
];

export const columns = [
  {
    title: 'name',
    dataIndex: 'name',
    key: 'name',
  },
  {
    title: 'height',
    dataIndex: 'height',
    key: 'height',
  },
  {
    title: 'mass',
    dataIndex: 'mass',
    key: 'mass',
  },
  {
    title: 'hair_color',
    dataIndex: 'hair_color',
    key: 'hair_color',
  },
  {
    title: 'skin_color',
    dataIndex: 'skin_color',
    key: 'skin_color',
  },
  {
    title: 'eye_color',
    dataIndex: 'eye_color',
    key: 'eye_color',
  },
  {
    title: 'birth_year',
    dataIndex: 'birth_year',
    key: 'birth_year',
  },
  {
    title: 'gender',
    dataIndex: 'gender',
    key: 'gender',
  },
  {
    title: 'homeworld',
    dataIndex: 'homeworld',
    key: 'homeworld',
  },
  {
    title: 'films',
    dataIndex: 'films',
    key: 'films',
  },
  {
    title: 'species',
    dataIndex: 'species',
    key: 'species',
  },
  {
    title: 'vehicles',
    dataIndex: 'vehicles',
    key: 'vehicles',
  },
  {
    title: 'starships',
    dataIndex: 'starships',
    key: 'starships',
  },
  {
    title: 'url',
    dataIndex: 'url',
    key: 'url',
  },
  { title: 'Action', key: 'action', width: 200 },
];
