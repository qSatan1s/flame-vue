import { apiInstance } from './base';

export const getPeopleList = () => apiInstance.get('/people');
