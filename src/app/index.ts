// FIXME не рекомендую к использованнию эту библиотеку :)
import Antd from 'ant-design-vue';

import { createApp } from 'vue';
import App from './index.vue';
import { router, store } from './providers';

export const app = createApp(App).use(Antd).use(router).use(store);
