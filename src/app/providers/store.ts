import { createStore } from 'vuex';

import { peopleModel } from 'entities/people';
import { searchModel } from 'entities/search';
import { isDevEnv } from 'shared/config';

export const store = createStore({
  strict: !!isDevEnv,
  modules: {
    [peopleModel.NAMESPACE]: peopleModel.module,
    [searchModel.NAMESPACE]: searchModel.module,
  },
});
