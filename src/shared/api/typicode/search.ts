import { apiInstance } from './base';

export const getSearchPeople = (text: string) => apiInstance.get(`/people/?search=${text}`);
