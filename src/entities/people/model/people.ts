import { typicodeApi } from 'shared/api';
import type { IPeople } from 'shared/api';

export const NAMESPACE = 'people';

const IS_NAMESPACED = true;

// FIX ME
// было бы ещё свободное время я бы написал интерфейс для стейта
export const module = {
  namespaced: IS_NAMESPACED,
  state: {
    data: {},
    isDetailsLoading: true,
    favorite: [],
    filterName: {},
  },
  getters: {
    getPeopleList: (state: any) => state.data?.results,
    getDetailsLoading: (state: any) => state.isDetailsLoading,
    getFilterPeople: (state: any) => state.filterName,
    getFavorite: (state: any) => state.favorite,
  },
  mutations: {
    setLocalStorage: (state: any) => {
      localStorage.setItem('favorites', JSON.stringify(state.favorite));
    },
    setFavorite: (state: any, favorites: Array<IPeople>) => {
      state.favorite = favorites;
    },
    filterPeople: (state: any, id: string) => {
      state.filterName = state.data.results.find((curPeople: IPeople) => curPeople.name === id);
    },

    setDetailsLoading: (state: any, isLoading: boolean) => {
      state.isDetailsLoading = isLoading;
    },

    toggleFavorite: (state: any, { id, isFavorite = false }: {id: string, isFavorite: boolean}) => {
      const person = state.data.results.find((curPeople: IPeople) => curPeople.url === id);
      person.isFavorite = isFavorite || !person?.isFavorite;
      if (person.isFavorite) {
        state.favorite.push(person.url);
      } else {
        state.favorite = state.favorite.filter((item:string) => item !== id);
      }
      module.mutations.setLocalStorage(state);
    },

    addPeopleToList: (state: any, response: any) => {
      state.data = {
        ...state.date, ...response?.data,
      };
    },
  },
  actions: {
    getPeopleList: async ({ commit }: any) => {
      commit('setDetailsLoading', true);
      try {
        commit('addPeopleToList', await typicodeApi.people.getPeopleList());
      } finally {
        commit('setDetailsLoading', false);
      }
    },
  },
};

const withPrefix = (name: string) => IS_NAMESPACED ? `${NAMESPACE}/${name}` : name;
export const actions = {
  getPeopleList: withPrefix('getPeopleList'),
};

export const mutations = {
  toggleFavorite: withPrefix('toggleFavorite'),
  filterPeople: withPrefix('filterPeople'),
  setFavorite: withPrefix('setFavorite'),
};

export const getters = {
  getPeopleList: withPrefix('getPeopleList'),
  getFilterPeople: withPrefix('getFilterPeople'),
  getIsPeopleList: withPrefix('getIsPeopleList'),
  getDetailsLoading: withPrefix('getDetailsLoading'),
  getFavorite: withPrefix('getFavorite'),
};
