import { favorites } from './favorites';
import Routing from './index.vue';
import { peopleByID, peoples } from './peoples';

export const routes = [
  { path: '/', component: () => import('./home') },
  { path: '/peoples', component: peoples },
  { path: '/peoples/:id', component: peopleByID, props: true },
  { path: '/favorites', component: favorites },
];

export { Routing };
