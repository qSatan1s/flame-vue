interface IPeople {
    name: string;
    height: string;
    mass: number;
    hair_color: string;
    skin_color: string;
    eye_color: string;
    birth_year: string;
    gender: string;
    homeworld: string;
    films: Array<string>;
    species: Array<string>;
    vehicles: Array<string>;
    starships: Array<string>;
    created: Date;
    edited: Date;
    url: string;
    isFavorite?: boolean;
}

export type { IPeople };
