import { typicodeApi } from 'shared/api';

export const NAMESPACE = 'search';

const IS_NAMESPACED = true;
export const module = {
  namespaced: IS_NAMESPACED,
  state: {
    search: [],
    isDetailsLoading: false,
  },
  getters: {
    getSearchResult: (state: any) => state.search,
  },
  mutations: {
    setResultSearch: (state: any, response: any) => {
      state.search = response?.data?.results;
    },

    setDetailsLoading: (state: any, isLoading: any) => {
      state.isDetailsLoading = isLoading;
    },

  },
  actions: {
    getSearchPeople: async ({ commit }: any, params: string) => {
      commit('setDetailsLoading', true);
      try {
        commit('setResultSearch', await typicodeApi.search.getSearchPeople(params));
      } finally {
        commit('setDetailsLoading', false);
      }
    },
  },
};

const withPrefix = (name: string) => IS_NAMESPACED ? `${NAMESPACE}/${name}` : name;

export const actions = {
  getSearchPeople: withPrefix('getSearchPeople'),
};

export const getters = {
  getSearchResult: withPrefix('getSearchResult'),
};
